import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {HttpModule} from '@angular/http';
import{MyDataService} from './my-data.service';
import { HttpClientModule } from '@angular/common/http';
import { MainpageComponent } from './mainpage/mainpage.component';
import { RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    
    RouterModule.forRoot([
      {
         path: 'mainpage',
         component: MainpageComponent
      }
   ])

  ],
  providers: [MyDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
