import { Injectable } from '@angular/core';
import {HttpClient } from "@angular/common/http";
import { map } from 'rxjs/operators';




@Injectable({
  providedIn: 'root'
})
export class MyDataService {
  httpdata;
  
  constructor(private http:HttpClient) { }
    

callService(){
  return this.http.get<any[]>('http://jsonplaceholder.typicode.com/users').pipe(map(data => data));
}

fetchData(callback) {
  this.callService().subscribe( restItems => {   callback.converttoarray(restItems) })//original
  
  }

}
